<?php
include('Header.php');
?>
<div class="wrapper" >
    <div class="container mt-5">
        <h4 class="T" > Sale Return</h4>

        <div class="d-print-none form-group ">
            <input list="OrderProduct" placeholder="Product" id="OrderProducts">
            <datalist id="OrderProduct" style="padding: 5px">
                
                 <?php 
             $result = mysqli_query($con,"SELECT * FROM product order by ProductName asc");

              $data =array();
              while($row = mysqli_fetch_assoc($result))
              {
                ?>
                <option id='<?php echo $row['id']?>' sold='<?php echo $row['Sold']?>' pp='<?php echo $row['PurchasePrice']?>' name='<?php echo $row['Price']?>'  value='<?php echo $row['ProductName']?>'><?php echo $row['Price']?></option>
                <?php
              }
             ?>
            </datalist>
            <label>Rate:</label>
            <input placeholder="Rate" id="Rate">
            <label>Quantity:</label>
            <input type="number" name="OrderQuantity" id="OrderQuantity" value="1" placeholder="Quantity" min="1" oninput="validity.valid||(value='');">
            <input type="button" name="AddButton" id="AddButton" value="Add" class="btn btn-primary" >
        </div>

        <div>
            <table class="wid table table-bordered table-hover"name="mytable" id="mytable">
                <thead class="bg-primary text-white">
                <tr>
                    <th scope="col">Product</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">UnitRate</th>
                    <th scope="col">Total Amount</th>
                    <th scope="col" class="d-print-none" >Delete</th>
                </tr>
                </thead>
                <tbody id="OrderTable">
                     <?php
                         $result = mysqli_query($con,"SELECT rtemp.id, rtemp.product_id, rtemp.qty, rtemp.rate, rtemp.total, product.ProductName FROM rtemp INNER JOIN product ON product.id= rtemp.product_id ");
                       
                          $data =array();
                          while($row = mysqli_fetch_assoc($result))
                          { ?>
                        <tr id='<?php echo $row['id']?>' >
                            <td style="display: none"><?php echo $row['product_id']?></td>
                            <td><?php echo $row['ProductName']?></td>
                            <td><?php echo $row['qty']?></td>
                            <td><?php echo $row['rate']?></td>
                            <td><?php echo $row['total']?></td>
                            <td class='d-print-none'> <input type='button' id='<?php echo $row['id']?>' Value='X' class='Delete btn btn-danger '> </td>
                            </tr>
                          <?php }
                      ?>
                </tbody>
            </table>
        </div>
        <div style="margin-top: 5px; float: right;">
            
                	<div id="CustomerName" style="margin-bottom: 10px">
                        <input list="CustomerOrder" placeholder="Customer" id="CustomerOrders">
                        <datalist id="CustomerOrder"  style="padding: 20px" class="in">
                             <?php
                             $result = mysqli_query($con,"SELECT * FROM customer order by CustomerName asc");

                             while($row = mysqli_fetch_assoc($result))
                              {?>
                                <option sr='<?php echo $row['id']?>' value='<?php echo $row['CustomerName']?>' > <?php echo $row['CustomerAddress']?> </option>
                                <?php
                              }
                             
                        ?>
                        </datalist>
            
                	</div>
                    <div id="SalesManName" style="margin-bottom: 10px">
                        <input list="salesman" placeholder="SalesMan" id="salesmans">
                        <datalist id="salesman"  style="padding: 20px" class="in">
                              <?php

                             $result = mysqli_query($con,"SELECT * FROM salesman order by name asc");

                              while($row = mysqli_fetch_assoc($result))
                              {?>
                                <option id='<?php echo $row['id']?>' value='<?php echo $row['name']?>' > "<?php echo $row['address']?>" </option>
                              <?php }
                          ?>
                        </datalist>
            
                    </div>
                    
                    <div id="DateSelector" style="margin-bottom:10px"> 
                    	<input class="col-sm" type="date" min="2018-03-01" id="Calender" value="<?php echo date('Y-m-d');?>" name="Calender" >
                	</div>
                   
                    
                	<div id="Total" style="margin-bottom:10px">
                    	<b class="col-sm2"> Total Amount:</b>
                    	<b class="col-sm2" id="TotalAmount">0</b>
               		</div>

                      <div id="Paid" style="margin-bottom:10px">
                      <label>Return:</label>  <input class="col-sm2" placeholder="Return Amount" value="0" type="number" min="0" id="received">
                    </div>
                <button type="submit" id="SaveButton" class="btn-primary btn"> Save </button>
        </div>
    </div>
</div>
</body>
</html>


<script type="text/javascript">
    
    function TotalAmountCalculator()
    {
        var ta = 0;
        var TotalRows = document.getElementById("OrderTable").rows.length;

        for( i = 0; i<TotalRows; i++)
        {
            PN = document.getElementById("OrderTable").rows[i].cells.item(4).innerHTML;
            ta = parseFloat(PN)+ta;
        }
        document.getElementById("TotalAmount").innerHTML=ta;
    }

    //Creating Order
    $(document).ready(function()

    {
        TotalAmountCalculator();
        $("#AddButton").click(function Order()//Function to Select Product And Quantity
        {
            var product_id = $("#OrderProduct option[value='" + $('#OrderProducts').val()+ "']").attr('id');
            var check = 0;
            $('#OrderTable tr').each(function(row,tr){

                    if($(tr).find('td:eq(0)').text() == product_id)
                    {
                        check++;
                    }
                });
            if (check == 0) 
            {
              var table = $("#OrderTable");
            var Flag = 2;
            var OrderQuantity = $("#OrderQuantity").val();//Getting OrderQuantity
            var sold = $("#OrderProduct option[value='" + $('#OrderProducts').val()+ "']").attr('sold');
            
            if (parseInt(sold) < parseInt(OrderQuantity)) 
            {
                alert("Out of Stock!");
                return;
            }

            var table = $("#OrderTable");
            
            var Price = $('#Rate').val();
            
            var OrderQuantity = $("#OrderQuantity").val();//Getting OrderQuantity
            var Total = Number(Price) * Number(OrderQuantity);//Total For One Item
         
           

            //MinusStock(product_id,OrderQuantity);
            TempData(product_id,OrderQuantity,Price,Total);//Temprary Data Table To Save Invoice/Order Tempraroy
        }
        else
            {
                var errMsg = "Product Already exists in the list";
                        alert(errMsg);
                        location.reload();
            }

        });
    });
    function TempData(product_id,OrderQuantity,Price,Total)
    {
        
        var Flag = 30;
        $.ajax({
            url: 'add-rtemp.php', //url from where we get data accesing DataBase
            data: {product_id:product_id, OrderQuantity:OrderQuantity, Price:Price,Total:Total,Flag:Flag},//passing data to php page in which php will send data to Database
            type: 'POST',
            success:function(data)
            {
                location.reload();
            }
        });
    }
  
    //Delete Function
    $(document).on('click','.Delete',function Delete(){
        var del_id= $(this).attr('id');
        var $ele = $(this).parent().parent();
         var product_id = $(this).parent().siblings(":eq(0)").text();
        var ReAddQuantity = $(this).parent().siblings(":eq(2)").text();
        var Flag = 32;

        $ele.fadeOut().remove();

        DelTem(del_id);
    });
    function DelTem(del_id)
    {
        var	Flag = 33;

        $.ajax({
            type:'POST',
            url:'StockManagment.php',
            data:{del_id:del_id,Flag:Flag},
            success: function(data){
                 location.reload();
            }

        });
    }
    /*Saving Inoice To DB*/
  $(document).ready(function()
    {
        $("#SaveButton").click(function Order()//Function to Select Product And Quantity
            {
              
                var table_data = [];
                var invoice_data = [];

              
                     $(this).attr("disabled", true);
                var inv = {

                  'customer_id' : $("#CustomerOrder option[value='" + $('#CustomerOrders').val()+ "']").attr('sr'),
                  'salesman_id' : $("#salesman option[value='" + $('#salesmans').val()+ "']").attr('id'),
                  'Amount'      : $("#TotalAmount").text(),
                  'received'        : $("#received").val(),
                  'Date'        : $("#Calender").val(),
                };
                    invoice_data.push(inv);
                   
                   $('#OrderTable tr').each(function(row,tr){

                    if($(tr).find('td:eq(0)').text() == "")
                    {

                    }
                    else
                    {
                        var details = 
                        {
                         'product_id' : $(tr).find('td:eq(0)').text(),
                         'quantity' : $(tr).find('td:eq(2)').text(),
                         'rate' : $(tr).find('td:eq(3)').text(),
                         'total_rate' : $(tr).find('td:eq(4)').text(),
                        };
                        table_data.push(details);
                    }
                });
                    
                    var sr = 0;
                    $.ajax({
                        url: 'returnInvoiceSave.php', //url from where we get data accesing DataBase
                        data: {'invoice_data':invoice_data,'table_data':table_data},//passing data to php page in which php will send data to Database
                        dataType: 'json',
                        type: 'POST',
                        cache:false,
                        success:function(data){
                                
                                alert("Invoice Successfully Saved");
                                 callurl(data.d2);
                                DelTemperory();

                        }
                    });
                }

        
        )
    });

    function callurl(sr)
    {
        
        var url = "view_returninvoice.php?id="+sr;
        window.location.href = url;
    }

    function CustomerManagment(cname,TotalAmount)
    {
        $.ajax({


            url: 'CustomerManagmet_returninvoice.php', //url from where we get data accesing DataBase
            data: {cname:cname,TotalAmount:TotalAmount},//passing data to php page in which php will send data to Database
            type: 'POST',
            success:function(data){
                //displaing received msg into div ID as #result
            }
        });
    }

    function DelTemperory()
    {
        var Flag = 34;
        $.ajax({
            type:'POST',
            url:'StockManagment.php',
            data:{Flag:Flag},
            success: function(data){
            }

        });
    }

  //Script is to Hovar/Mark opened page in navbar
    $(function(){
        $('a').each(function(){
            if ($(this).prop('href') == window.location.href) {
                $(this).addClass('active'); $(this).parents('li').addClass('active');
            }
        });
    });
</script>
</html>