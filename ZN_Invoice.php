
<script>
function printPage() {
    window.print();
    window.location.href = "invoice.php";
}
</script>
  <style type="text/css">
    table {  border-collapse: separate; border-radius:15px; border-spacing:0}
    label { margin: 5px 0px 5px 45px; }
  </style>
</head>
<?php include 'HeaderOnPrintPage.php';?>
<div class="Container" style="max-width: 750px; margin: 0px auto;">

<?php
 $ID=$_GET['id'];
$Date = '';
$query2 = "SELECT  invoice.id, invoice.customer_id, invoice.Amount, invoice.date, invoice.salesman_id, customer.CustomerName,customer.CustomerAddress,customer.CustomerPhone, salesman.name  From invoice INNER JOIN customer ON customer.id = invoice.customer_id INNER JOIN salesman ON salesman.id = invoice.salesman_id WHERE invoice.id = $ID ";
//echo $query2;
if ($result2=mysqli_query($con,$query2))
  {  // Fetch one and one row
  while ($row2=mysqli_fetch_assoc($result2))
  {
     $CustomerAddress = $row2['CustomerAddress'];          
     $CustomerName = $row2['CustomerName'];
     $Date= $row2['date']; 
     $Amount=$row2['Amount'];
}
}

?>
<div class="d-none d-print-block" style="line-height: 10px; text-align: center;">

   <!--  <img src="img/logo.png" style="width:180px; height: 100px;"> -->
    <h2 id="h-name">ZN Trading Co. Pvt. Ltd</h1>
    <h3> Sale Invoice </h2>
    <h4 id="phone1">04237640971-2</h3>
    <h5 id="h-address">104 Mcload Road, Lahore</h4>
</div>
<div style="margin: 0 auto; width: 100%; margin-top: 20px;background: #101010;border: 1px;border-style: solid; border-radius: 15px ;margin-bottom: 10px;color: white;">
<table style="width: 100%;">
  <tr class="row">
   <td class="col-md-4"><label style="color: white;">Invoice Number:<b style="margin-left: 10px;color: white;">INV-<?php echo $ID;?></b></label></td>
   <td class="col-md-4"><label style="color: white;">Address :<b style="margin-left: 10px;color: white;"><?php echo $CustomerName;?></b></label></td>
   <td class="col-md-4"><label style="color: white;">Date :<b style="margin-left: 10px;color: white;" ><?php echo $Date;?></b></label></td>
  </tr>
</table>
</div>
  
  <div style="background-color: #ffffff">
  <table class="table table1"  style="font-size: 14px; border-collapse: separate; background-color: #fff; border:10px; border-style: solid; border-color: #ebebe0" >
      <tr class="tr">
        <th style="text-align: center;" > Sr  </th>
        <th style="text-align: center;" > Product Name  </th>
        <th style="text-align: center;" > Quantity      </th>
        <th style="text-align: center;" > Unit Rate     </th>
        <th style="text-align: center;" > Product Rate  </th>   
      </tr>
  <tbody id="ordertable">
   <?php
   
        $query = "SELECT invoicedetail.*, product.ProductName  FROM invoicedetail INNER JOIN product ON product.id = invoicedetail.product_id WHERE invoice_id = $ID order by invoicedetail.id asc";

  if ($result=mysqli_query($con,$query))
    {  // Fetch one and one row
    while ($row=mysqli_fetch_assoc($result))
      {
  ?>
         <tr class="tr" >
              <td ></td>
              <td ><?php echo $row['ProductName'];?>  </td> 
              <td ><?php echo $row['OrderQuantity'];?></td> 
              <td ><?php echo $row['UnitRate']; ?>    </td>
              <td ><?php echo $row['ProductRate']?>   </td> 
          </tr>     
       <?php
    }
  }

  ?>
  </tbody>
</table>
</div>


<div style="width: 100%; min-height: 70px;margin: 0 auto;  margin-top: 10px; ">
  <table style="width: 35%; border-top: 3px solid #e0ccff;float: right; font-size: 18px;line-height: 15px;">
    <tr style="background-color: white">
     <td><label style="margin-left: 16%; font-weight: bold; font-family:monospace, serif; ">Amount <b style="background-color:#ebebe0;font-weight: normal; margin-left: 10px;" id="am" ><?php echo $Amount ?></b></label></td>
     </tr>

  </table>
</div>
<?php require ('InvoiceReportFooter.php');?>
  <div id="AddressToCut" class=" d-none d-print-block" style="font-size: 45px"><b><?php echo $CustomerAddress;?></b> </div>
</div>
</body>
<script type="text/javascript">
   var ajax = new XMLHttpRequest();
    var method = "Get";
    var url = "get_company_detail.php";
    var asyn = true;
    //Ajax open XML Request
    ajax.open(method,url,asyn);
    ajax.send();

    ajax.onreadystatechange = function displayCustomer()
    {
        if(this.readyState == 4 && this.status == 200)
        {
            var data = JSON.parse(this.responseText);
            console.log(data);
            var d = "";

            for (var i = 0; i<data.length ; i++)
            {
                var sr = data[i].sr;
                var name = data[i].name;
                var address = data[i].address;
                var phone1 = data[i].phone1;
                var phone2 = data[i].phone2;
                var mobile1 = data[i].mobile1;
                var mobile2 = data[i].mobile2;
                var fax = data[i].fax;
                var web = data[i].web;
                var email = data[i].email;
                var facebook = data[i].facebook;
                var slogan = data[i].slogan;
                
                
            }
            document.getElementById("h-name").innerHTML = name;
            document.getElementById("h-address").innerHTML = address;
            document.getElementById("s-email").innerHTML = email;
            document.getElementById("s-web").innerHTML = web;
            document.getElementById("s-mobile2").innerHTML = mobile2;
            document.getElementById("s-mobile1").innerHTML = mobile1;
            document.getElementById("s-ph1").innerHTML = phone1;
            
        }
    }

	var ta = 0;
	var TotalRows = document.getElementById("ordertable").rows.length;
	for( i = 0; i<TotalRows; i++)
    {
    	pa = document.getElementById("ordertable").rows[i].cells.item(4).innerHTML;
    	ta = ta +  parseInt(pa);
    }
   	document.getElementById("am").innerHTML = ta;
</script>
</html>
