<?php
include('Header.php');
?>

<!-- Modal Start -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title T">Edit</h4>
            </div>
            <div class="modal-body">
                <input type="text" name="EditName" class="col-sm-4 mr-2"  id="EditName" placeholder="Name" style='text-transform:uppercase'>
                <br>
                <input type="Number" name="EditName" class="col-sm-4 mr-2"  id="EditPhone" placeholder="Phone">
                <br>
                <input type="text" name="" style='text-transform:uppercase' class="col-sm-4 mr-2"  id="EditAddress" placeholder="Address">
                <br>
                <input type="hidden" name="sr" class="col-sm-2 mr-2"  id="Sr">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-right" value="Save" id="SaveUpdated"> Save</button>
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!-- Modal End -->

<div class="wrapper">
    <div class="container mt-5">
        <h4 class="T" > Manager</h4>
        <div class="form-group">
            <input type="text" class="col-sm-2 mr-2" name="" onkeyup="SearchByName()"placeholder="Managers Name" id="i-salesmanname" style='text-transform:uppercase'>
            <input type="number" class="col-sm-2 mr-2" name="PhoneNumber"  placeholder="Phone Number" id="i-phonenumber">
            <input type="text" class="col-sm-2 mr-2" name="" onkeyup="SearchByAddress()" placeholder="Address" id="i-address" style='text-transform:uppercase'>
            <button type="submit" name="" class="btn btn-primary" id="btnAddsalesman">Add</button>
        </div>

        <table class='wid table table-bordered table-hover' style="">
            <thead class='bg-primary text-white'>
            <tr>
                <th scope='col'>Managers Name</th>
                <th scope='col'>Phone</th>
                <th scope='col'>Address</th>
                
                <!--<th scope='col'>Update</th> -->
                <!-- <th scope='col'>Delete</th> -->
            </tr>
            </thead>
            <tbody id="Table">
            </tbody>
        </table>
    </div>
</div>
</body>


<script type="text/javascript">
    //Send Data To DB
    $(document).ready(function()
    {
        $("#btnAddsalesman").click(function AddSalesMan()
        {
            //Getting Value From Input Fields
            var salesmanName = $("#i-salesmanname").val();
            var PhoneNumber = $("#i-phonenumber").val();
            var Address = $("#i-address").val();
            //Ajax Call to PHP Send data that we get from input fields into variable and passing to php page
            $.ajax({
                url: 'Salesman-SendData.php', //url from where we get data accesing DataBase
                data: {salesmanName:salesmanName, PhoneNumber:PhoneNumber, Address:Address},//passing data to php page in which php will send data to Database
                type: 'POST',
                success:function(data){
                    //displaing received msg into div ID as #result
                    alert(data);
                    location.reload();

                }
            });
        });
    });
    //Display Function For Customer
    var ajax = new XMLHttpRequest();
    var method = "Get";
    var url = "SalesMan-DisplayData.php";
    var asyn = true;
    //Ajax open XML Request
    ajax.open(method,url,asyn);
    ajax.send();
    //ajax call for display
    ajax.onreadystatechange = function Display()
    {
        if(this.readyState == 4 && this.status == 200)
        {
            var data = JSON.parse(this.responseText);
            console.log(data);
            var d = "";
            for (var i = 0; i<data.length ; i++)
            {
                var Sr = data[i].sr;
                var Name = data[i].name;    //CustomerSr,CustomerName,CustomerPhone,CustomerAddress,TotalAmount,PaidAmount
                var Phone = data[i].phone;
                var Address = data[i].address;
                var sales = data[i].sales;

                d += "<tr data-id='"+Sr+"'>";
                d +="<td data-target='Name' class='Name' >"+ Name + "</a></td>";
                d +="<td data-target='Phone' id='Phone' class='Phone' >"+ Phone + "</td>";
                d +="<td data-target='Address' id='Address' class='Address' >"+Address + "</td>";
                /*
                d +="<td> <a href='#' data-role='update' data-id='"+Sr+"'>Update</a> </td>";*//*Calling Model Through JS Function*/
                //d +="<td> <input type='button' id='"+CustomerName+"' Value='Delete' class='Delete'> </td>";
                d +="</tr>";
            }
            document.getElementById("Table").innerHTML = d;
        }
    }
    //ModalCalling For Customer Update Through Update Button
    $(document).on('click','a[data-role=update]',function ModleCall(){
        var id = $(this).data('id');

        var Name = $(this).parent().siblings("td:first").text();
        var Phone =  $(this).parent().siblings(":eq(1)").text();
        var Address = $(this).parent().siblings(":eq(2)").text();
        var Sales = $(this).parent().siblings(":eq(3)").text();
        
        $("#EditName").val(Name);
        $('#EditPhone').val(Phone);
        $('#EditAddress').val(Address);
        
        $('#Sr').val(id);
        $('#myModal').modal('toggle');
    });

    //Update Customer Using Ajax
    $('#SaveUpdated').click(function Update(){

        var sr = $("#Sr").val();
        var Name = $("#EditName").val();
        var Phone = $("#EditPhone").val();
        var Address = $("#EditAddress").val();
        
        $.ajax({
            url : 'UpdateSalesman.php',
            method : 'post',
            data : {sr:sr, Name:Name, Phone:Phone, Address:Address},
            success : function(response){
                //console.log(response);
                alert(response);
                location.reload();
            }
        });
    });
    //Delete Customer Using Ajax
    $(document).on('click','.Delete',function Delete(){

        var Conf = confirm("Do You Realy Want To Delete?");
        if(Conf == true)
        {
            var del_id= $(this).attr('id'); //id Getting From Delete Button For Specific Row
            var $ele = $(this).parent().parent();

            $.ajax({
                type:'POST',
                url:'Customer-Delete.php',
                data:{'del_id':del_id},
                success: function(data){
                    alert(data);
                    $ele.fadeOut().remove();
                }

            });
        }

    });
</script>
<script>
  //Script is to Hovar/Mark opened page in navbar
    $(function(){
        $('a').each(function(){
            if ($(this).prop('href') == window.location.href) {
                $(this).addClass('active'); $(this).parents('li').addClass('active');
            }
        });
    });
</script>
<script type="text/javascript">
    function SearchByName() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("i-salesmanname");
  filter = input.value.toUpperCase();
  table = document.getElementById("Table");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
function SearchByAddress() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("i-address");
  filter = input.value.toUpperCase();
  table = document.getElementById("Table");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>
</html>