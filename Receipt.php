<?php
include('Header.php');
?>
<div class="wrapper">
    <div class="contain container mt-5">
	<h4 class="T" >RECEIVE PAYMENT RECEIPT</h4>
	
	<div class="Customerinfo" style="text-align: center;">
        <input type="Date" placeholder="Select Date" value="<?php echo date('Y-m-d');?>" class=" in" id="datepicker">
            <div id="CustomerName" style="margin-bottom: 10px">
                <input list="CustomerOrder" sr="-1" placeholder="Select Customer" id="CustomerOrders">
                <datalist id="CustomerOrder"  style="padding: 20px" class="in">
                <?php
                     $result = mysqli_query($con,"SELECT * FROM customer order by CustomerName asc");

                     while($row = mysqli_fetch_assoc($result))
                      {?>
                        <option sr='<?php echo $row['id']?>' value='<?php echo $row['CustomerName']?>' > <?php echo $row['CustomerAddress']?> </option>
                        <?php
                      }
                     
                ?>
            </datalist>
            </div>
        <input type="number" placeholder="Received Amount" class="in" id="ReceivedAmount">
        <input type="text" name="Cash/ChequeNo"class=" in" placeholder="Cash/Cheque No" id="receivedtype">
        <input type="text" name="bankname"class=" in" placeholder="Bank Name" id="i-bankname">
        <input type="Date" placeholder="Select Date" class=" in" id="bankdatepicker">
        <input type="text" name="being" class="in" id="being" placeholder="Being">
        <input type="submit" class="btn btn-primary in royalbutton" id="btnSave" value="SAVE" style="color: white">
	</div>
</div>
</div>
</body>
<script type="text/javascript">
    $(document).ready(function()
    {
        $("#btnSave").click(function AddDetail()
        {
            $(this).attr("disabled", true);
            var customer_id = $("#CustomerOrder option[value='" + $('#CustomerOrders').val()+ "']").attr('sr');
            var date = $("#datepicker").val();
            var ReceivedAmount = $("#ReceivedAmount").val();
            var receivedtype = $("#receivedtype").val();
            var bankname = $("#i-bankname").val();
            var bankdatepicker = $("#bankdatepicker").val();
            var being = $("#being").val();
            $.ajax({
                url:'ReceiptSave.php', //url from where we get data accesing DataBase
                    data: {customer_id:customer_id, ReceivedAmount:ReceivedAmount,receivedtype:receivedtype,date:date, bankdatepicker:bankdatepicker,being:being,bankname:bankname},//passing data to php page in which php will send data to Database
                    type: 'POST',
                    success:function(data){

                            callurl(data);
                            }
                            
                    });           
        });
    });
    function callurl(sr)
    {
        var url = "PrintReceipt.php?id="+sr;
        window.location.href = url;
    }

  //Script is to Hovar/Mark opened page in navbar
    $(function(){
        $('a').each(function(){
            if ($(this).prop('href') == window.location.href) {
                $(this).addClass('active'); $(this).parents('li').addClass('active');
            }
        });
    });
</script>


</html>