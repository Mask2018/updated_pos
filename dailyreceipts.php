    <?php
      include('Header.php');
    ?>
    
<body>
    <div class="wrapper">
    <div class="container mt-5">
      <h4 class="T">RECEIVING PAYMENT RECEIPTS</h4>
      <div class="form-group">
          <input type="Date" class="datepicker" onchange="SearchByDate()" placeholder="Select Date" id="datepicker">
          <input type="text" name="Name" onkeyup="SearchByName()" placeholder="SearchByName" id="SearchByName"> 
      </div>
      <table style="" class="table table-bordered table-hover" id="tabledata">
        <thead class="" style="background-color:#007bff; color: white">
          <tr>
            <th scope="col" class="in" style="width: 15%">Receipt#</th>
            <th scope="col" class="in" style="width: 15%">Cutomer Name</th>
            <th scope="col" class="in" style="width: 15%">Date</th>
            <th scope="col" class="in" style="width: 15%">Paid Amount</th>
          </tr>
        </thead>
        <tbody id="ReportTable" style="text-align: center;">
          
  <?php

    include 'Connection.php';
    $query = "SELECT receipt.* , customer.CustomerName FROM receipt INNER JOIN customer ON customer.id = receipt.customer_id ORDER BY id DESC";
      
        if ($result=mysqli_query($con,$query))
        {  // Fetch one and one row
          while ($row=mysqli_fetch_assoc($result))
          {
            ?>
          <tr class="tr" >
              
              <td > <a href="PrintReceipt.php?id=<?php echo $row['id'];?>"</a> <?php echo $row['id'];?></td> 
              <td ><?php echo $row['CustomerName'];?> </td> 
              <td ><?php echo $row['dat']; ?>      </td>
              <td ><?php echo $row['ReceivedAmount']?>  </td>
             
          </tr>     
            <?php
                  }
                } 
            ?>

        </tbody>
      </table>
    </div>
    </div>
  </body>

 
  <script type="text/javascript">
    function SearchByDate() 
    {
      var input, filter, table, tr, td, i;
      input = document.getElementById("datepicker");
      filter = input.value.toUpperCase();
      table = document.getElementById("tabledata");
      tr = table.getElementsByTagName("tr");
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[2];
        if (td) {
          if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }       
      }
    }
    function SearchByName() 
    {
      var input, filter, table, tr, td, i;
      input = document.getElementById("SearchByName");
      filter = input.value.toUpperCase();
      table = document.getElementById("tabledata");
      tr = table.getElementsByTagName("tr");
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
          if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }       
      }
    }
  </script>
<script>
  //Script is to Hovar/Mark opened page in navbar
    $(function(){
        $('a').each(function(){
            if ($(this).prop('href') == window.location.href) {
                $(this).addClass('active'); $(this).parents('li').addClass('active');
            }
        });
    });
</script>
</html>