<?php
include('Header.php');
?>
<div class="wrapper" >
    <div class="container mt-5">
        <h4 class="T" > Purchase Invoice</h4>

        <div class="d-print-none form-group ">
            <input list="OrderProduct" id="OrderProducts" onchange="getPurchasePrice()">
            <datalist id="OrderProduct" style="padding: 5px">
                


            <?php 
             $result = mysqli_query($con,"SELECT * FROM product order by ProductName asc");

              $data =array();
              while($row = mysqli_fetch_assoc($result))
              {
                ?>
                <option id='<?php echo $row['id']?>' name='<?php echo $row['PurchasePrice']?>' 
                        value='<?php echo $row['ProductName']?>'>           
                </option>
                <?php
              } ?>

            </datalist>

            <label class="lbl" >Order Quantity:</label>

            <input type="number" name="OrderQuantity" id="OrderQuantity" value="1" placeholder="Quantity" min="1" oninput="validity.valid||(value='');">


            <label class="lbl" >Purchase Price:</label>
            <input type="number" name="Purchase Price" id="PurchasePrice" value="1" placeholder="Purchase Price"> 

            <input type="button" name="AddButton" id="AddButton" value="Add" class="btn btn-primary">
        </div>

        <div class="table">
            <table class="wid table table-bordered table-hover"name="mytable" id="mytable">
                <thead class="bg-primary text-white">
                <tr>
                    <th scope="col">Product</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">UnitRate</th>
                    <th scope="col">Total Amount</th>
                    <th scope="col" class="d-print-none" >Delete</th>
                </tr>
                </thead>
                <tbody id="OrderTable">

                     <?php
                         $result = mysqli_query($con,"SELECT purchasetemp.id, purchasetemp.product_id, purchasetemp.Quantity, purchasetemp.Price, purchasetemp.Total, product.ProductName FROM purchasetemp INNER JOIN product ON product.id=purchasetemp.product_id ");
                       
                          $data =array();
                          while($row = mysqli_fetch_assoc($result))
                          { ?>
                        <tr id='<?php echo $row['id']?>' >
                            <td style="display: none"><?php echo $row['product_id']?></td>
                            <td><?php echo $row['ProductName']?></td>
                            <td><?php echo $row['Quantity']?></td>
                            <td><?php echo $row['Price']?></td>
                            <td><?php echo $row['Total']?></td>
                            <td class='d-print-none'> <input type='button' id='<?php echo $row['id']?>' Value='X' class='Delete btn btn-danger '> </td>
                            </tr>
                          <?php }
                      ?>

                </tbody>
            </table>
        </div>
        <div style="margin-top: 5px; float: right;">
            
                	<div id="VendorName">
                        <input list="vendor" sr="-1" placeholder="Select Vendor" id="vendors">
                        <datalist id="vendor"  style="padding: 20px" class="in">
                          <?php
                             $result = mysqli_query($con,"SELECT * FROM vendor order by VendorName asc");

                             while($row = mysqli_fetch_assoc($result))
                              {?>
                                <option id='<?php echo $row['id']?>' value='<?php echo $row['VendorName']?>' > <?php echo $row['VendorAddress']?> </option>
                                <?php
                              }
                             
                        ?>
                	</div>
                    <!-- hide ref -->
                    <div id="Total" style="margin-bottom:10px;">
                        <b class="col-sm2"> Refrence No:</b>
                        <input class="col-sm2" type="text" id="i-refrenceno" value="0">
                    </div>
                    <div id="total" style="margin-bottom:10px">
                        <b class="col-sm2"> Total Amount:</b>
                        <b class="col-sm2" id="total_amount">0</b>
                    </div>
                    <div id="DateSelector" style="margin-bottom:10px"> 
                    	<input class="col-sm" type="date" min="2018-03-01" id="calender" value="<?php echo date('Y-m-d');?>" name="calender" >
                	</div>
                   
                <button type="submit" id="SaveButton" class="btn-primary btn"> Save </button>
        </div>
    </div>
</div>
</body>
</html>

 <script type="text/javascript">
        // ya ni ha ruk ik min
    function getPurchasePrice(){
        var x = $("#OrderProduct option[value='" + $('#OrderProducts').val()+ "']").attr('name');
        $("#PurchasePrice").val(x);
        
    }



TotalAmountCalculator();
 
 function TotalAmountCalculator()
     {
        var ta = 0;
        var TotalRows = document.getElementById("OrderTable").rows.length;

        for( i = 0; i<TotalRows; i++)
        {
            PN = document.getElementById("OrderTable").rows[i].cells.item(4).innerHTML;
            ta = parseFloat(PN)+ta;
        }
        ta= ta.toFixed(2);
        document.getElementById("total_amount").innerHTML=ta;
    }

    
    //Creating Order
    $(document).ready(function()

    {
        $("#AddButton").click(function Order()//Function to Select Product And Quantity
        {

            var product_id = $("#OrderProduct option[value='" + $('#OrderProducts').val()+ "']").attr('id');

           
            var price = $("#PurchasePrice").val();
            var qty = $("#OrderQuantity").val();//Getting OrderQuantity
            var total = parseFloat(price) * parseFloat(qty);//Total For One Item
            total = total.toFixed(2);
            
            /*UpdateStock(product_id ,qty);*/
            TempData(product_id, qty,price,total);//Temprary Data Table To Save Invoice/Order Tempraroy


        });
    });
    function TempData(product_id, qty,price,total)
    {
        
        var Flag = 10;
        $.ajax({
            url: 'StockManagment.php', //url from where we get data accesing DataBase
            data: {product_id:product_id, qty:qty, price:price, total:total, Flag:Flag},//passing data to php page in which php will send data to Database
            type: 'POST',
            success:function(data)
            {
                location.reload();
            }
        });
    }
    function UpdateStock(product_id ,qty)
    {
        var Flag = 11;
        $.ajax({
            url: 'StockManagment.php', //url from where we get data accesing DataBase
            data: {product_id:product_id, qty:qty,Flag:Flag},//passing data to php page in which php will send data to Database
            type: 'POST',
            success:function(data)
            {
            }
        });
    }
    //Delete Function
    $(document).on('click','.Delete',function Delete(){
        var del_id= $(this).attr('id');
        var $ele = $(this).parent().parent();
        var product_id = $(this).parent().siblings(":eq(0)").text();
        var qty = $(this).parent().siblings(":eq(1)").text();
        var Flag = 12;

        $ele.fadeOut().remove();

        DelTem(del_id);
        
        $.ajax({
            url: 'StockManagment.php', //url from where we get data accesing DataBase
            data: {product_id:product_id, qty:qty, Flag:Flag},//passing data to php page in which php will send data to Database
            type: 'POST',
            success:function(data)
            {
                location.reload();
            }
        });
    });
    function DelTem(del_id)
    {
        var	Flag = 13;

        $.ajax({
            type:'POST',
            url:'StockManagment.php',
            data:{del_id:del_id,Flag:Flag},
            success: function(data){

            }

        });
    }
    /*Saving Inoice To DB*/
    $(document).ready(function()
    {
        $("#SaveButton").click(function Order()//Function to Select Product And Quantity
            {
              
                var table_data = [];
                var invoice_data = [];

              
                     $(this).attr("disabled", true);
                var inv = {

                  'vendor_id' : $("#vendor option[value='" + $('#vendors').val()+ "']").attr('id'),
                  'Amount'      : $("#total_amount").text(),
                  'Date'        : $("#calender").val(),
                  'refrence_no' : $("#i-refrenceno").val(),
                };
                    invoice_data.push(inv);
                  console.log(invoice_data);
                   $('#OrderTable tr').each(function(row,tr){

                    if($(tr).find('td:eq(0)').text() == "")
                    {

                    }
                    else
                    {
                        var details = 
                        {
                         'product_id' : $(tr).find('td:eq(0)').text(),
                         'quantity' : $(tr).find('td:eq(2)').text(),
                         'rate' : $(tr).find('td:eq(3)').text(),
                         'total_rate' : $(tr).find('td:eq(4)').text(),
                        };
                        table_data.push(details);
                    }
                });
                    
                    var sr = 0;
                    $.ajax({
                        url: 'PurchaseInvoiceSave.php', //url from where we get data accesing DataBase
                        data: {'invoice_data':invoice_data,'table_data':table_data},//passing data to php page in which php will send data to Database
                        dataType: 'json',
                        type: 'POST',
                        cache:false,
                        success:function(data){
                                alert("Invoice Successfully Saved");
                                callurl(data.d2);
                                DelTemperory();

                        }
                    });
                }

        
        )
    });

    function callurl(sr)
    {
        var url ="PurchaseInvoiceReport.php?id="+sr;
                    window.location.href = url;
    }

    function VendorManagment(Address,TotalAmount,PaidAmount,Balance)
    {
        $.ajax({


            url: 'VendorManagmet.php', //url from where we get data accesing DataBase
            data: {Address:Address,TotalAmount:TotalAmount,PaidAmount:PaidAmount, Balance:Balance},//passing data to php page in which php will send data to Database
            type: 'POST',
            success:function(data){
                //displaing received msg into div ID as #result
            }
        });
    }

    function DelTemperory()
    {
        var Flag = 14;
        $.ajax({
            type:'POST',
            url:'StockManagment.php',
            data:{Flag:Flag},
            success: function(data){
            }

        });
    }
 
    function ReceivedFunction() 
    {
        var TotalAmount = $("#TotalAmount").text();
        TotalAmount = parseFloat(TotalAmount);
        var  n= TotalAmount.toFixed(2);

        var rec = document.getElementById("Received").value;
        var Claim = document.getElementById("ClaimAmount").value;
        var Balance = n - rec - Claim;

        /*if (rec > TotalAmount) {
            document.getElementById("Received").value = TotalAmount; //to set by default total amount.
            document.getElementById("Received").style.background = "grey";
        }*/
        document.getElementById("Balance").innerHTML = Balance;
    }
    function Balance()
    {
        var TotalAmount = $("#TotalAmount").text();
        TotalAmount = parseFloat(TotalAmount);
        var  n= TotalAmount.toFixed(2);

        var Claim = document.getElementById("ClaimAmount").value;
        var rec = document.getElementById("Received").value;
        var Balance = n - Claim -rec;
        document.getElementById("Balance").innerHTML = Balance;
    }


  //Script is to Hovar/Mark opened page in navbar
    $(function(){
        $('a').each(function(){
            if ($(this).prop('href') == window.location.href) {
                $(this).addClass('active'); $(this).parents('li').addClass('active');
            }
        });
    });
</script>
</html>