 <?php include 'Connection.php';?>
<!doctype html>
<html >
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    
     <link rel="shortcut icon" href="img/OT.ico" />
     <title>IMPOS</title>

    <!-- Bootstrap core CSS -->
    <script src="jquery-3.3.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
<!-- <script src="Functions.js"></script>     -->
    <link href="css/fontsgooglappies.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link type="text/javascript" href="css/jquery-ui.css" >
    <!-- Custom styles for this template -->
    <link href="style.css" rel="stylesheet">
    <link href="css/Newcss.css" rel="stylesheet">

  

  <style type="text/css">
ul .dropdown-menu{
  padding: 5px;
  width: 180%; 
}
.navbar-dark .navbar-nav .nav-link{
  color: lightgrey;
  font-size: 1.45vw;
} 
.dropdown ul li{
  padding: 5px;
  font-size: 1.22vw;
  border-bottom: 2px solid #0062cc;
}
.dropdown ul li a{
  color:lightgrey;
  text-decoration:none;
}

.navbar-dark .navbar-nav .nav-link:hover{
  color: white;
}
.dropdown ul li:hover{
  border-bottom: 2px solid white;
}
.dropdown ul li a:hover{
 color: white;
}

.dropdown ul li:last-child{
  border-bottom: none;
}
thead th {
    position: sticky;
    position: -webkit-sticky;
    top: 0;
    z-index: 999;
    background-color: #007bff;
    color: #fff;
}
#navbarSupportedContent{
  letter-spacing: 1.3px;
}

    .dis{
      display: none;
    }
    input{
      padding: 10px;
      box-shadow: 1px 1px 25px rgba(0, 0, 0, 0.35);
    }
    button{
      box-shadow: 1px 1px 25px rgba(0, 0, 0, 0.35);
    }
    select{
      padding: 10px;
      box-shadow: 1px 1px 25px rgba(0, 0, 0, 0.35);
    }
    table{
      max-width: 688px;
      text-align: center;
      box-shadow: 1px 1px 25px rgba(0, 0, 0, 0.35);
    }
    html,body{font-family: arial;
      height: 100%}
    label{margin-left: 10px;font-weight: 600}
    .OrderTable td, .OrderTable th{ border: 1px solid black; border-top: 1px solid black;border-collapse: separate; border-spacing:0;}
    label{
      font-weight: bold;
    }
    .in{
  width: 50%;
  border-radius: 10px;
  margin-bottom: 10px;
  padding: 10px;
  text-align: center;
}
    .T{
    margin: 0 auto;
    width: 35%;
    margin-top: 20px;
    background: #007bff;
    border: 1px;
    border-style: solid;
    border-radius: 15px;
    margin-bottom: 10px;
    color: white;
    text-align: center;
    padding: 10px;
    box-shadow: 1px 1px 25px rgba(0, 0, 0, 0.35);
    }
    .dis{
      display: none;
    }
    /*Styling for Sale/Purcase Order pages*/
    #OrderTable {
            counter-reset: rowNumber;
        }

        #OrderTable tr {
            counter-increment: rowNumber;
        }

        #OrderTable tr td:first-child::before {
            content: counter(rowNumber);
            min-width: 1em;
            margin-right: 0.5em;
        }
        #nav_bar .active {
    color:            #F8F8F8;
    background-color: #4f81bd;
}
.wid
{
  min-width: 1110px;
}
.verticalline {
    border-left: 1px solid black;
    height: 160px;
    float: left;
    margin: 0px 5px 0px 5px;
}
.dropdownbg{
  color: white;
  text-align: center;
}
.dropdown li{
  border-bottom: 1px solid white;

}
@media print{
  body{ background-color:#FFFFFF; background-image:none; color:#000000 }
  #ad{ display:none;}
  #leftbar{ display:none;}
  #contentarea{ width:100%;}
}

  </style>
  </head>

<body>
   <!-- Preloader -->
        <div id="loading">
            <div id="loading-center">
                <div id="loading-center-absolute">
                    <div class="object" id="object_one"></div>
                    <div class="object" id="object_two"></div>
                    <div class="object" id="object_three"></div>
                    <div class="object" id="object_four"></div>
                </div>
            </div>
        </div><!--End off Preloader -->
  
   <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container">  
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="add-product.php">Product</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="add-customer.php">Customer</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="add-vendor.php">Vendor</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="add-salesman.php">Managers</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="Expensis.php">Expense</a>
          </li>
          
          <li class="nav-item">
             <div class="dropdown">
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Receipts
                <span class="caret"></span></button>
                <ul class="dropdown-menu" style="background-color: #007bff;">
                  <li><a href="Receipt.php" class="dropdownbg">Receiving Receipt</a></li>
                  <li><a href="PaymentReceipt.php" class="dropdownbg">Payment Receipt</a></li>
                </ul>
              </div> 
          </li>
          
          <li class="nav-item">
             <div class="dropdown">
                <button style="margin-left: 15px" class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Invoices
                <span class="caret"></span></button>
                <ul class="dropdown-menu" style="margin-left: 15px;background-color: #007bff ;">
                  <li><a href="invoice.php" class="dropdownbg">Sale Invoice</a></li>
                  <li><a href="purchaseinvoice.php" class="dropdownbg">Purchase Invoice</a></li>
                  <li><a href="invoice_salereturn.php" class="dropdownbg">Sale Return</a></li>
                </ul>
              </div> 
          </li>
          <li class="nav-item">
             <div class="dropdown">
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="margin-left: 15px">Reports
                <span class="caret"></span></button>
                <ul class="dropdown-menu" style="margin-left: 15px;background-color: #007bff ;">
                  
                <li><a  href="expense_summary.php" class="dropdownbg">Expens Reports</a></li>
                <li><a  href="CompleteLedger.php" class="dropdownbg">Ledger</a></li>
                <li><a  href="PaidReceiptsReport.php" class="dropdownbg">Paid Receipts</a></li>
                <li><a  href="PL.php" class="dropdownbg">Profit/Loss</a></li>
                <li><a  href="PurchaseReports.php" class="dropdownbg">Purchase Reports</a></li>
                <li><a href="daily.php" class="dropdownbg">Sale Reports</a></li>
                <li><a href="salesummery.php" class="dropdownbg">Sale Summery</a></li>
                <li><a href="StockReport.php" class="dropdownbg">StockReport</a></li>
                <li><a href="allreturninvoices.php" class="dropdownbg">Return Invoices</a></li> 
                <li><a  href="dailyreceipts.php" class="dropdownbg">Receiving P. Receipts</a></li>
                 
                 
                  <!--  -->
                </ul>
              </div> 
          </li>
          <li class="nav-item">
              
          </li>
          <li class="nav-item">
              
          </li>
          <li class="nav-item">
              
          </li>
        </ul>
      </div>
    </div>
  </nav>