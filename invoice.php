<?php
include('Header.php');
?>
<div class="wrapper" >
    <div class="container mt-5">
        <h4 class="T" > Sale Invoice</h4>

        <div class="d-print-none form-group ">
            <input list="OrderProduct" placeholder="Product" id="OrderProducts">
            <datalist id="OrderProduct" style="padding: 5px">
                
            <?php 
             $result = mysqli_query($con,"SELECT * FROM product order by ProductName asc");

              $data =array();
              while($row = mysqli_fetch_assoc($result))
              {
                ?>
                <option id='<?php echo $row['id']?>' stock='<?php echo $row['Stock']?>' pp='<?php echo $row['PurchasePrice']?>' name='<?php echo $row['Price']?>'  value='<?php echo $row['ProductName']?>'><?php echo $row['Price']?></option>
                <?php
              }
             ?>
            

            </datalist>

            <!-- hide rate  start-->
            <label >Rate:</label>
            <input type="number" min="1" placeholder="Rate" id="i-rate">
            <!-- hide rate end -->
            <label>Quantity:</label>
            <input type="number" name="OrderQuantity" id="OrderQuantity" value="1" placeholder="Quantity" min="1" oninput="validity.valid||(value='');">
            <input type="button" name="AddButton" id="AddButton" value="Add" class="btn btn-primary" >
        </div>

        <div>
            <table class="wid table table-bordered table-hover"name="mytable" id="mytable">
                <thead class="bg-primary text-white">
                <tr>
                    <th scope="col">Product</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">UnitRate</th>
                    <th scope="col">Total Amount</th>
                    <th scope="col">Profit</th>
                    <th scope="col" class="d-print-none" >Delete</th>
                </tr>
                </thead>
                <tbody id="OrderTable">
                    <?php
                         $result = mysqli_query($con,"SELECT tem.id, tem.product_id, tem.Quantity, tem.Rate, tem.Total,tem.Profit, product.ProductName FROM tem INNER JOIN product ON product.id=tem.product_id ");
                       
                          $data =array();
                          while($row = mysqli_fetch_assoc($result))
                          { ?>
                        <tr id='<?php echo $row['id']?>' >
                            <td style="display: none"><?php echo $row['product_id']?></td>
                            <td><?php echo $row['ProductName']?></td>
                            <td><?php echo $row['Quantity']?></td>
                            <td><?php echo $row['Rate']?></td>
                            <td><?php echo $row['Total']?></td>
                            <td><?php echo $row['Profit']?></td>
                            <td class='d-print-none'> <input type='button' id='<?php echo $row['id']?>' Value='X' class='Delete btn btn-danger '> </td>
                            </tr>
                          <?php }
                      ?>

                </tbody>
            </table>
        </div>
        <div style="margin-top: 5px; float: right;">
            
                	<div id="CustomerName" style="margin-bottom: 10px">
                        <input list="CustomerOrder" sr="-1" placeholder="Select Customer" id="CustomerOrders">
                        <datalist id="CustomerOrder"  style="padding: 20px" class="in">
                       
                        <?php
                             $result = mysqli_query($con,"SELECT id,CustomerName,CustomerAddress FROM customer order by CustomerName asc");

                             while($row = mysqli_fetch_assoc($result))
                              {?>
                                <option sr='<?php echo $row['id']?>' value='<?php echo $row['CustomerName']?>' > <?php echo $row['CustomerAddress']?> </option>
                                <?php
                              }
                             
                        ?>
                    </datalist>
                	</div>
                    <div id="SalesManName" style="margin-bottom: 10px">
                        <input list="salesman"  sr="-1" id="salesmans" placeholder="Select Manager">
                        <datalist id="salesman"  style="padding: 20px" class="in">
                        <?php

                             $result = mysqli_query($con,"SELECT * FROM salesman order by name asc");

                              while($row = mysqli_fetch_assoc($result))
                              {?>
                                <option id='<?php echo $row['id']?>' value='<?php echo $row['name']?>' > "<?php echo $row['address']?>" </option>
                              <?php }
                          ?>
                        </datalist>
                    </div>
                    
                    <div id="DateSelector" style="margin-bottom:10px"> 
                    	<input class="col-sm" type="date" min="2018-03-01" id="Calender" value="<?php echo date('Y-m-d');?>" name="Calender" >
                	</div>
                    <div id="TotalProf" style="margin-bottom:10px;">
                        <b class="col-sm2"> Total Profit:</b>
                        <b class="col-sm2" id="TotalProfit">0</b>
                    </div>
                    
                	<div id="Total" style="margin-bottom:10px">
                    	<b class="col-sm2"> Total Amount:</b>
                    	<b class="col-sm2" id="TotalAmount">0</b>
               		</div>
               		 <div id="Claim" style="margin-bottom:10px;display: none;">
                    	
                    	<input class="col-sm2" type="number" placeholder="DISCOUNT"  min="0" onkeyup="Balance()" id="ClaimAmount" value="0" >
                    </div>
                     <div id="Paid" style="margin-bottom:10px;display: none;">
                    	<input class="col-sm2" placeholder="ReceivedAmount" type="number" min="0" id="Received" oninput="ReceivedFunction()" value="0">
                	</div>
                	<div id="Bal" style="margin-bottom:10px;display: none;">
                   		<b class="col-sm2"> Balance:</b>
                   		<b class="col-sm2" id="Balance" ></b>
              		</div>

              
                   
                        <button type="submit" id="SaveButton" class="btn-primary btn"> Save </button>
        </div>
    </div>
</div>
</body>
</html>

<script type="text/javascript">

    // for shortcut keys
     $( document ).on( 'keydown', function ( e ) {
      if ( e.keyCode === 17 ) { //ctrl key code
        $( "#OrderProducts" ).focus();
      }
      if ( e.keyCode === 32 ) { //space key code
        $( "#OrderQuantity" ).focus();
      }
      if ( e.keyCode === 13 ) { //Enter key code
        $("#AddButton").click();
    }
        if(e.keyCode === 16){

        $("#i-rate").focus();
      }
    });
   
TotalAmountCalculator();
   
    function TotalAmountCalculator()
    {
        var ta = 0;
        var pro = 0;
        var TotalRows = document.getElementById("OrderTable").rows.length;

        for( i = 0; i<TotalRows; i++)
        {
            PN = document.getElementById("OrderTable").rows[i].cells.item(4).innerHTML;
            PROF = document.getElementById("OrderTable").rows[i].cells.item(5).innerHTML;
            ta = parseFloat(PN)+parseFloat(ta);
            ta = ta.toFixed(2);
            pro = parseFloat(PROF)+parseFloat(pro);
            pro = pro.toFixed(2);
        }
        document.getElementById("TotalAmount").innerHTML=ta;
        document.getElementById("TotalProfit").innerHTML=pro;
        document.getElementById("Balance").innerHTML=ta;
        ReceivedFunction();
        Balance();
    }
     function ReceivedFunction() {
        var TotalAmount = $("#TotalAmount").text();
        var rec = document.getElementById("Received").value;
        var clm = document.getElementById("ClaimAmount").value;
        var Balance = parseFloat(TotalAmount) - rec-clm;

        document.getElementById("Balance").innerHTML = Balance;
    }
    function Balance()
    {
        var TotalAmount = $("#TotalAmount").text();
        var Claim = document.getElementById("ClaimAmount").value;
        var rec = document.getElementById("Received").value;

        var Balance = parseFloat(TotalAmount) - Claim -rec;
        document.getElementById("Balance").innerHTML = Balance;
    }


    function disable()
    {
        var op = document.getElementById("OrderProduct").getElementsByTagName("option");

        for (var i = 0; i < op.length; i++) {
            if (op[i].title <= "1") {
                op[i].disabled = true;

            }
        }

    }
    function CheckStock()
    {
        var e = document.getElementById("OrderProduct");//Getting Product From Select Tag
        var Product = e.options[e.selectedIndex].text;//Getting Product Through Option
        var Quantity = e.options[e.selectedIndex].title;//Geting Price By The id of the Option Tag
        var OrderQuantity = $("#OrderQuantity").val();//Getting OrderQuantity
        var Order = parseFloat(OrderQuantity);
        
        if (Quantity < Order)
        {
            alert(Product+" in Stock is = "+Quantity);
            location.reload();
            /*document.getElementById("OrderQuantity").innerHTML = Quantity;*/
        }

    }
    //Creating Order
    $(document).ready(function()

    {
        $("#AddButton").click(function Order()//Function to Select Product And Quantity
        {
            var product_id = $("#OrderProduct option[value='" + $('#OrderProducts').val()+ "']").attr('id');
            var check = 0;
            $('#OrderTable tr').each(function(row,tr){

                    if($(tr).find('td:eq(0)').text() == product_id)
                    {
                        check++;
                    }
                });
            if (check == 0) 
            {
              var table = $("#OrderTable");
            var Flag = 2;
            var OrderQuantity = $("#OrderQuantity").val();//Getting OrderQuantity
            var stock = $("#OrderProduct option[value='" + $('#OrderProducts').val()+ "']").attr('stock');
            
            if (parseInt(stock) < parseInt(OrderQuantity)) 
            {
                alert("Out of Stock!");
                return;
            }

            var pp = $("#OrderProduct option[value='" + $('#OrderProducts').val()+ "']").attr('pp');
            
            var Price = $("#i-rate").val();//$("#OrderProduct option[value='" + $('#OrderProducts').val()+ "']").attr('name');
            
            //var PurchasePrice = $("#OrderProduct option[value='" + $('#OrderProducts').val()+ "']").attr('id');
            
            var Total = parseFloat(Price) * parseFloat(OrderQuantity);//Total For One Item
            var Profit = (parseFloat(Price)-parseFloat(pp))*parseFloat(OrderQuantity);
            Total = Total.toFixed(2);
            Profit = Profit.toFixed(2);
            
      
                 $.ajax({
                url: 'StockManagment1.php', //url from where we get data accesing DataBase
                data: {product_id:product_id, OrderQuantity:OrderQuantity, Price:Price,Total:Total,Profit:Profit},//passing data to php page in which php will send data to Database
                type: 'POST',
                success:function(data)
                {

                    location.reload();
                }
                });

            document.getElementById("OrderQuantity").value = 0;
            //MinusStock(product_id,OrderQuantity);
            TotalAmountCalculator();
            /*TempData(Product,OrderQuantity,Price,Total,TotalAmount,TotalProfit,Profit);*///Temprary Data Table To Save Invoice/Order Tempraroy  
            }
            else
            {
                var errMsg = "Product Already exists in the list";
                        alert(errMsg);
                        location.reload();
            }
            


        });
    });
  
    function MinusStock(product_id,OrderQuantity,PurchasePrice)
    {
        var Flag = 0;
        $.ajax({
            url: 'StockManagment.php', //url from where we get data accesing DataBase
            data: {product_id:product_id, OrderQuantity:OrderQuantity, Flag:Flag},//passing data to php page in which php will send data to Database
            type: 'POST',
            success:function(data)
            {
                
            }
        });
    }
    //Delete Function
    $(document).on('click','.Delete',function Delete(){
        var del_id= $(this).attr('id');
        var $ele = $(this).parent().parent();

        var product_id = $(this).parent().siblings(":eq(0)").text();
        var ReAddQuantity = 0;
        var Flag = 1;

        $ele.fadeOut().remove();

            DelTem(del_id);
    });
    function DelTem(del_id)
    {
        var	Flag = 3;

        $.ajax({
            type:'POST',
            url:'StockManagment.php',
            data:{del_id:del_id,Flag:Flag},
            success: function(data){
                location.reload();
            }

        });
    }
    /*Saving Inoice To DB*/
    $(document).ready(function()
    {
        $("#SaveButton").click(function Order()//Function to Select Product And Quantity
            {
              
                var table_data = [];
                var invoice_data = [];

              
                     $(this).attr("disabled", true);
                var inv = {

                  'customer_id' : $("#CustomerOrder option[value='" + $('#CustomerOrders').val()+ "']").attr('sr'),
                  'salesman_id' : $("#salesman option[value='" + $('#salesmans').val()+ "']").attr('id'),
                  'Amount'      : $("#TotalAmount").text(),
                  'TotalProfit' : $("#TotalProfit").text(),
                  'Paid'        : 0,
                  'ClaimAmount' : 0,
                  'Balance'     : $("#TotalAmount").text(),
                  'Date'        : $("#Calender").val(),
                };
                    invoice_data.push(inv);
                   
                   $('#OrderTable tr').each(function(row,tr){

                    if($(tr).find('td:eq(0)').text() == "")
                    {

                    }
                    else
                    {
                        var details = 
                        {
                         'product_id' : $(tr).find('td:eq(0)').text(),
                         'quantity' : $(tr).find('td:eq(2)').text(),
                         'rate' : $(tr).find('td:eq(3)').text(),
                         'total_rate' : $(tr).find('td:eq(4)').text(),
                         'profit' :  $(tr).find('td:eq(5)').text(),
                        };
                        table_data.push(details);
                    }
                });
                    
                    var sr = 0;
                    $.ajax({
                        url: 'InvoiceSave.php', //url from where we get data accesing DataBase
                        data: {'invoice_data':invoice_data,'table_data':table_data},//passing data to php page in which php will send data to Database
                        dataType: 'json',
                        type: 'POST',
                        cache:false,
                        success:function(data){
                                
                                alert("Invoice Successfully Saved");
                                 callurl(data.d2);
                                DelTemperory();

                        }
                    });
                }

        
        )
    });

    function callurl(sr)
    {
        
        var url = "ZN_Invoice.php?id="+sr;
        window.location.href = url;
    }


    function DelTemperory()
    {
        var Flag = 4;
        $.ajax({
            type:'POST',
            url:'StockManagment.php',
            data:{Flag:Flag},
            success: function(data){
           
            }

        });
    }

  //Script is to Hovar/Mark opened page in navbar
    $(function(){
        $('a').each(function(){
            if ($(this).prop('href') == window.location.href) {
                $(this).addClass('active'); $(this).parents('li').addClass('active');
            }
        });
    });
</script>
</html>