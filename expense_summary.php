<?php
      include('Header.php');
?>
   

   <div class="wrapper">
   <div class="container mt-5">
   <h4 class="T" >EXPENS Report</h4>
<input type="Date" class="datepicker" onchange="SearchByDate()" placeholder="Select Date" id="datepicker">
      <div class="table">
      	<table class='wid table table-bordered table-hover' style="text-align: center;">
          <thead class='bg-primary text-white'>
              <tr>
              <th scope='col'>Expense</th>
              <th scope='col'>Description</th>
              </tr>
          </thead>
          <tbody id="ProductTable">
                <?php
       
    include 'Connection.php';
     $query = mysqli_query($con,"SELECT Dat, SUM(Price) as Price FROM expense GROUP BY Dat");

        if ($query)
        {  // Fetch one and one row
          while ($row=mysqli_fetch_assoc($query))
          {
            ?>
          <tr class="tr" >
              
              <td > <a href="expens_day.php?date=<?php echo $row['Dat'];?>" </a> <?php echo $row['Dat'];?></td> 
              <td ><?php echo $row['Price']; ?> </td>
 
          </tr>     
        <?php
          }
        } 
        ?>
          </tbody>  
        </table>
      </div>
        <div id="result"></div>
    </div>
    </div>
  </body>
 
  <script type="text/javascript">
    //Send Data To DB
    $(document).ready(function()
    {
      $("#btnAdd").click(function AddProduct()
      {
        $(this).attr("disabled", true);
        //Getting Value From Input Fields
        var Expense = $("#Expense").val();
        var Description = $("#Description").val();
        var Price = $("#Price").val();
        var Dat = $("#Calender").val();
        //Ajax Call to PHP Send data that we get from input fields into variable and passing to php page
        $.ajax({
          url: 'Expense-SendData.php', //url from where we get data accesing DataBase
          data: {Expense:Expense, Description:Description,Price:Price,Dat:Dat},//passing data to php page in which php will send data to Database
          type: 'POST',
          success:function(data){
          //displaing received msg into div ID as #result
          alert(data);
          location.reload();

          } 
        });
      }); 
  //Get Data From DB and Display it into a table
 
  
    $(document).on('click','a[data-role=update]',function ModleCall(){
      var id = $(this).data('id');
      //getting prices through row id to show specific data to modle
      var ProductName = $(this).parent().siblings("td:first").text();
      var BikeName =  $(this).parent().siblings(":eq(1)").text(); 
      var PurchasePrice = $(this).parent().siblings(":eq(2)").text();
      var Price = $(this).parent().siblings(":eq(3)").text();
      var Quantity =0;

      $("#EditProductName").val(ProductName);
      $('#EditBikeName').val(BikeName);
      $('#EditPurchasePrice').val(PurchasePrice);
      $('#EditPrice').val(Price);
      $('#EditQuantity').val(Quantity);
      $('#ProductSr').val(id);
      $('#myModal').modal('toggle');
    });
    //Update Product
    $('#SaveUpdated').click(function Update(){

      var sr = $("#ProductSr").val();
      var ProductName = $("#EditProductName").val();
      var BikeName = $("#EditBikeName").val();
      var PurchasePrice = $("#EditPurchasePrice").val();
      var Price = $("#EditPrice").val();
      var Quantity = $("#EditQuantity").val();
      
      $.ajax({
        url : 'UpdateProduct.php',
        method : 'post',
        data : {sr:sr, ProductName:ProductName, BikeName:BikeName,PurchasePrice:PurchasePrice, Price:Price, Quantity:Quantity},
        success : function(response){
          //console.log(response);
          alert(response);
          location.reload();
        }
      });
    });

    $(document).on('click','.Delete',function Delete(){
        
        var Conf = confirm("Do You Realy Want To Delete?");
        if(Conf == true)
        {

        var del_id= $(this).attr('id');
        var $ele = $(this).parent().parent();
            $.ajax({
            type:'POST',
            url:'ap-DeleteData.php',
            data:{'del_id':del_id},
            success: function(data){
                    $ele.fadeOut().remove();
                 }

            });
          }
        });
  });

    $(function(){
        $('a').each(function(){
            if ($(this).prop('href') == window.location.href) {
                $(this).addClass('active'); $(this).parents('li').addClass('active');
            }
        });
    });
 function SearchByDate() 
    {
      var input, filter, table, tr, td, i;
      input = document.getElementById("datepicker");
      filter = input.value.toUpperCase();
      table = document.getElementById("ProductTable");
      tr = table.getElementsByTagName("tr");
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
          if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }       
      }
    }
</script>
</html>
